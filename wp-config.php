<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('WP_CACHE', true);
define( 'WPCACHEHOME', 'C:\xampp\htdocs\propuesta\wp-content\plugins\wp-super-cache/' );
define( 'DB_NAME', 'tiendadelsol' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '#$%W,jEVWw,kU2.HEM(2vQ%_vskB/&z+K}gJ@5`|{ma?WY#Z|px7($*{gqtS~unB' );
define( 'SECURE_AUTH_KEY',  ';XBD=sh` B3|V;MW?d~`_=[GJzQz6?@Pn@H+|0V t/!gNherBXRWVSl:H!d3vDv-' );
define( 'LOGGED_IN_KEY',    'c%h>WY-/[;?Z tbz>)AH[{$m{!^2<6:{3phGpN*9;%6RF/c1j=WG:<>&YDzHUZ:D' );
define( 'NONCE_KEY',        'rLI2c]jNaAAmj23)9zk;>-!l2^R8B`2:P8=2$n`#Yx9g/9l>Q{VgI;m>V-0Z_4?t' );
define( 'AUTH_SALT',        'O;:MIYU.r%]Hs=+cI`.WeFgMt:,{*a>oF@ *Lb[86T(vRisdLG)g98,7!U>abrqa' );
define( 'SECURE_AUTH_SALT', 'f?&mLYkb@1i)`cRC}c$!ZrJ?X.cPYMM-Mn6x.i=nn,?Oiu$E>0tok}K`<.)< 6#z' );
define( 'LOGGED_IN_SALT',   'lzFF`Jg.ZxurKB&qmt_4*4v4:7)uQdLR<6g3uR@!6Oh&#Y<3#Q@e4feFowb}v<b:' );
define( 'NONCE_SALT',       'z$ssqdhBJ4c2Eu3?9Y(UE0t56 0M]JPBJR^4hW}D[#2((dulqgMkL.;be>){{i/z' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );


/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
